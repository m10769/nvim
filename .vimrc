

" DEFAULT OPTIONS:
"   This options is from my lua/user/options.lua Neovim config


set ignorecase nobackup
set hidden confirm noshowmode
set splitbelow splitright number cursorline

set clipboard=unnamedplus
set termguicolors encoding=UTF-8
set timeoutlen=500 mouse=a

set expandtab nosmartindent breakindent linebreak
set shiftwidth=4 tabstop=4 formatoptions=1
set term=xterm-256color

let mapleader="\<space>"


" + remove the ~ at the end of the file
set fcs=eob:\ 

" + interface theme
colorscheme pablo

" + Highlight for text files
au BufWinEnter *.txt set ft=help



" Cool things to know... ;)
"
" + You can remove the background color with:
"       hi Normal guibg=NONE ctermbg=NONE
" + Use the `gf` command in a path string to open the file










" NO PLUGIN SETUP:
"   Ideas from:
"   + https://www.youtube.com/watch?v=XA2WjJbmmoM&ab_channel=thoughtbot


" + stop vim to use some VI features
set nocompatible

" + syntax and plugin for netrw (file manager)
filetype plugin on
syntax enable










" FUZZY FILE SEARCH:

" + add all subfolder in path (to use tabcomplete)
set path+=**
set path-=**/node_modules/**

" + show a options popup when using tabcomplete
set wildmenu

" Now you can create a keybind to fuzzy search betwen directories
nnoremap <leader>FF :find ./**/










" TAG JUMPING:

" + create a command to run ctags in current directory (install ctags)
command! MakeTags !ctags -R --exclude=.git --exclude=node_modules --exclude=test .

" Now you can use:
"   + ^]  to jump to a tag under cursor
"   + g^] to list all the tags related
"   + ^t  to go back in tag stack









" AUTOCOMPLETE:
"   Just some notes... See the documentation in |ins-completion|

"   + ^n   will search by anything specifed by the 'complete' option
"   + ^x^n for text words
"   + ^x^f for filenames (uses the path variable)
"   + ^x^] for tags only

" + show a list of the files in that folder
inoremap / /<c-x><c-f><c-p>
inoremap . .<c-n><c-p>
inoremap ( (<c-n><c-p>
inoremap [ [<c-n><c-p>
inoremap { {<c-n><c-p>









" FILE BROWSING:
"   Checkout the docs in |netrw-browse-maps|

" + tweaks for browsing in NETRW
let g:netrw_banner=0       " disable annoying banner
let g:netrw_liststyle=3    " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide=',\(^\|\s\s\)\zs\.\S\+'

" + mapping to open the explorer
nnoremap <leader>n :edit .<cr>











" CUSTOM MAPPINGS:
"   Thats my custom mappings, hope I helped you in some way ;3


" General Propose Mappings:

" + better <esc>
inoremap <c-l> <esc>
vnoremap <c-l> <esc>

" + to save a file
nnoremap <c-s> <esc>:w!<cr>
inoremap <c-s> <esc>:w!<cr>
vnoremap <c-s> <esc>:w!<cr>

" + to quit the vim
nnoremap <leader>Q :qa!<cr>
nnoremap <leader>q :q<cr>

" + fixing the strange vim behavior
nnoremap j gj
nnoremap k gk
nnoremap Y yg$


" Window Buffer And Tab Mappings Config:

" + create a new tab
nnoremap <leader>t :tabnew<cr>

" + navigate betwen windows
nnoremap <leader>h <c-w>h
nnoremap <leader>j <c-w>j
nnoremap <leader>k <c-w>k
nnoremap <leader>l <c-w>l

" + split the window
nnoremap <leader>s :vsplit<cr>
nnoremap <leader>i :split<cr>

" + window resize
nnoremap <left> :vertical resize +1<cr>
nnoremap <down> :resize -1<cr>
nnoremap <up> :resize +1<cr>
nnoremap <right> :vertical resize -1<cr>

" + navigate betwen buffers
nnoremap <tab> :bn<cr>
nnoremap <s-tab> :bp<cr>

" + buffer management using arrow keys
nnoremap <leader><down> :bdelete<cr>
nnoremap <leader><up> :ls<cr>

" + navigate betwen tabs
nnoremap <leader><tab> :tabnext<cr>
nnoremap <leader><s-tab> :tabprevius<cr>


" Hack And Unussual Mappings:

" + add a , and a ; at the end of the line
nnoremap <leader>, A,<esc>
nnoremap <leader>; A;<esc>

" + enable or desabe the numbers
nnoremap <leader>: :set number!<cr>

" + toggle the wordwrap option
nnoremap <leader><c-w> :set wrap!<cr>

" + change the indent width
nnoremap <leader>1 :setl expandtab tabstop=2 shiftwidth=2 softtabstop=2<cr>
nnoremap <leader>2 :setl expandtab tabstop=4 shiftwidth=4 softtabstop=4<cr>
nnoremap <leader>3 :setl expandtab tabstop=8 shiftwidth=8 softtabstop=8<cr>

" + autoindent the whole file
nnoremap <leader>gg mzgg=G"z

" + save the current vim session in vim_secs folder
nnoremap <leader>sm :mksession! ~/.dotfiles/vim_secs/

" + open a terminal buffer
nnoremap <leader>T :term<cr>
