local status_ok, packer = pcall(require, 'packer')

if not status_ok then
    print '[ ERROR] Can\'t load packer plugin manager'
    return
end


--  HACK: Simple tool to import the configuration using pcall() function

local function config_module(path)
    local module_status_ok, module = pcall(require, path)

    if module_status_ok then
        return module
    end
end


--  MAIN: Packer plugin list (without use() function from packer)

local plugins = {
    'kyazdani42/nvim-web-devicons',
    'wbthomason/packer.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-lua/popup.nvim',


    --  SECTION: User interface, syntax and theme

    {
        'nvim-treesitter/nvim-treesitter',
        config = config_module 'plugins.config.treesitter'
    },
    {
        'rmehri01/onenord.nvim',
        config = config_module 'plugins.config.onenord'
    },
    {
        'nvim-lualine/lualine.nvim',
        config = config_module 'plugins.config.lualine'
    },
    {
        'akinsho/bufferline.nvim',
        requires = 'kyazdani42/nvim-web-devicons',
        config =  config_module 'plugins.config.bufferline'
    },
    {
        'folke/todo-comments.nvim',
        config = config_module 'plugins.config.todo-comments'
    },


    --  SECTION: Language server protocol, snippets and autocompletion

    'honza/vim-snippets',

    {
        'mattn/emmet-vim',
        config = config_module 'plugins.config.emmet'
    },
    {
        'neoclide/coc.nvim',
        branch = 'release',
        config = config_module 'plugins.config.coc'
    },


    --  SECTION: Workflow and productivity hacks

    'MunifTanjim/nui.nvim',
    'tpope/vim-commentary',
    'tpope/vim-surround',

    {
        'windwp/nvim-autopairs',
        config = require 'nvim-autopairs'.setup {}
    },
    {
        'akinsho/toggleterm.nvim',
        config = require 'toggleterm'.setup {}
    },
    {
        'nvim-neo-tree/neo-tree.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', 'MunifTanjim/nui.nvim' },
        config = config_module 'plugins.config.neotree'
    },
    {
        'nvim-telescope/telescope.nvim',
        requires = 'nvim-lua/plenary.nvim',
        config = config_module 'plugins.config.telescope'
    },
    {
        'lewis6991/gitsigns.nvim',
        requires = 'nvim-lua/plenary.nvim',
        config = config_module 'plugins.config.gitsigns'
    }
}


--  HACK: Vimscript to run +PackerSync command when this file is saved

vim.cmd [[
    augroup packer_user_config
        autocmd!
        autocmd BufWritePost ~/.config/nvim/lua/plugins/plugins.lua source <afile> | PackerSync
    augroup end
]]



--  INFO: Use a loop to install all the plugins and render a floating windows for packer UwU

return packer.startup {
    function(use)
        for _, element in ipairs(plugins) do
            use(element)
        end

        if PACKER_BOOTSTRAP then
            require 'packer'.sync()
        end
    end,

    config = {
        display = {
            open_fn = function()
                return require 'packer.util'.float {
                    border = 'rounded'
                }
            end
        }
    }
}
