local status_ok, treesitter = pcall(require, 'nvim-treesitter.configs')

if not status_ok then
    print '[ ERROR] Can\'t load treesitter plugin'
    return
end


--  MAIN: Treesitter syntax and indent files setup

treesitter.setup {
    sync_install = true,

    highlight = {
        enable = true,
        disable = { 'html', 'markdown', 'vim' },
        additional_vim_regex_highlighting = true
    },

    indent = {
        enable = true,
        disable = { 'scss' }
    }
}
