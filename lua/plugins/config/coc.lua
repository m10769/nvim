--  MAIN: CoC config, using vimscript

vim.g.coc_snippet_next = '<tab>'
vim.g.coc_snippet_prev = '<s-tab>'

vim.cmd [[ imap ,, <Plug>(coc-snippets-expand-jump) ]]


--  DESC: Change the color palet from CoC diagnostics

vim.cmd [[ hi! CocErrorSign   guifg=#BF616A ]]
vim.cmd [[ hi! CocInfoSign    guifg=#5E81AC ]]
vim.cmd [[ hi! CocWarningSign guifg=#EBCB8B ]]
