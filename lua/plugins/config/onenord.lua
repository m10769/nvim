-- NOTE: Config from https://github.com/rmehri01/onenord.nvim


local status_ok, onenord = pcall(require, 'onenord')

if not status_ok then
    print '[ ERROR] Can\'t load onenord plugin'
    return
end


--  MAIN: Nord colorshceme config

onenord.setup {
    theme = 'dark',
    borders = false, -- Split window borders
    fade_nc = false, -- Fade non-current windows, making them more distinguishable

    styles = {
        comments = "italic",
        strings = "NONE",
        keywords = "NONE",
        functions = "italic",
        variables = "bold",
        diagnostics = "underline",
    },

    disable = {
        background = false, -- Disable setting the background color
        cursorline = false, -- Disable the cursorline
        eob_lines = true, -- Hide the end-of-buffer lines
    }
}
