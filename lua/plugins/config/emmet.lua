--  MAIN: Emmet config in vimscript

vim.g.user_emmet_install_global = false
vim.g.user_emmet_leader_key = '<'


local emmet_files = {
    '*.html', '*.css', '*.js', '*.scss', '*.sass', '*.md', '*.ts',  '*.jsx',  '*.tsx',
}


--  HACK: That will install Emmet in some especifc files

for _, element in pairs(emmet_files) do
    vim.cmd('au BufWinEnter ' .. element .. ' EmmetInstall')
end
