--  NOTE: setup from https://github.com/folke/todo-comments.nvim#%EF%B8%8F-configuration


local status_ok, todo_comments = pcall(require, 'todo-comments')

if not status_ok then
    print '[ ERROR] Can\'t load todo-comments plugin'
    return
end


--  MAIN: Todo comments colors and icon setup
--  NOTE: Default icons:       

return todo_comments.setup {
    signs = true,
    sign_priority = 8,
    merge_keywords = true,

    keywords = {
        TODO = { icon = '', color = '#88c0d0' },
        HACK = { icon = '', color = '#a3be8c', alt = { 'TIP', 'TEMP', 'TMP' }},
        PERF = { icon = '', color = '#b48ead', alt = { 'OPTIM', 'PERFORMANCE', 'OPTIMIZE' }},
        SEC  = { icon = '', color = '#4c566a', alt = { 'SECTION', 'DIV', 'DESC' }},
        FIX  = { icon = '', color = '#bf616a', alt = { 'FIXME', 'BUG', 'FIXIT', 'ISSUE', 'ERROR' }},
        WARN = { icon = '', color = '#ebcb8b', alt = { 'WARNING', 'CONSTRUCTOR', 'MAIN' }},
        NOTE = { icon = '', color = '#5e81ac', alt = { 'INFO', 'DOC', 'CHANGELOG', 'TODO' , 'TODO' , 'TODO'  }}
    },

    highlight = {
        before = '',
        keyword = 'wide',
        after = 'fg',
        pattern = [[.*<(KEYWORDS)\s*:]],
        comments_only = true,
        max_line_len = 400,
        exclude = {}
    },

    colors = {
        error = { 'DiagnosticError', 'ErrorMsg', '#DC2626' },
        warning = { 'DiagnosticWarning', 'WarningMsg', '#FBBF24' },
        info = { 'DiagnosticInfo', '#2563EB' },
        hint = { 'DiagnosticHint', '#10B981' },
        default = { 'Identifier', '#7C3AED' }
    },

    search = {
        command = 'rg',
        pattern = [[\b(KEYWORDS):]],

        args = {
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
        }
    }
}
