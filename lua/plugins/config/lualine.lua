-- Bubbles config for lualine
-- Author: lokesh-krishna
-- MIT license, see LICENSE for more details.


--  INFO: Hex color codes

local colors = {
    black  = '#2E3440',
    white  = '#c6c6c6',
    grey   = '#3F4758',
    cyan   = '#88c0d0',
    violet = '#b48ead',
    blue   = '#b48ead',
    red    = '#bf616a',
}


--  INFO: Lualine theme color scheme

local bubbles_theme = {
    insert = { a = { fg = colors.black, bg = colors.cyan } },
    visual = { a = { fg = colors.black, bg = colors.violet } },
    replace = { a = { fg = colors.black, bg = colors.violet } },

    normal = {
        a = { fg = colors.black, bg = colors.blue },
        b = { fg = colors.white, bg = colors.grey },
        c = { fg = colors.black, bg = colors.black },
    },

    inactive = {
        a = { fg = colors.white, bg = colors.black },
        b = { fg = colors.white, bg = colors.black },
        c = { fg = colors.black, bg = colors.black },
    },
}


--  MAIN: Lualine theme and elements setup

require('lualine').setup {
    tabline = {},
    extensions = { 'nvim-tree' },

    options = {
        theme = bubbles_theme,
        component_separators = '|',
        section_separators = { left = '', right = '' }
    },

    sections = {
        lualine_a = {
            { 'mode', separator = { left = '' }, right_padding = 2 },
        },

        lualine_b = { 'filename', 'branch' },
        lualine_c = { 'fileformat' },
        lualine_y = { 'filetype', 'progress' },
        lualine_z = {{
            'location',
            separator = { right = '' },
            left_padding = 2
        }},
    }
}
