local status_ok, bufferline = pcall(require, 'bufferline')

if not status_ok then
    print '[ ERROR] Can\'t load bufferline plugin'
    return
end


--  INFO: Hex color codes

local pallet = {
    elem_fg       = '#4c566a',
    selected_bg   = '#3b4252',
    error_fg      = '#bf616a',
    warning_fg    = '#ebcb8b',
    info_fg       = '#81a1c1',
    pick_f        = '#a3be8c',
    bar_bg        = '',
    bar_fg        = '',
    elem_bg       = '',
    selected_fg   = '',
}


--  INFO: Bufferline color scheme

local colors = {
    warning             = { guifg = pallet.warning_fg,  guibg = pallet.elem_bg,     guisp = pallet.warning_fg },
    error               = { guifg = pallet.error_fg,    guibg = pallet.elem_bg,     guisp = pallet.error_fg },
    info                = { guifg = pallet.info_fg,     guibg = pallet.elem_bg,     guisp = pallet.info_fg },

    warning_selected    = { guifg = pallet.warning_fg,  guibg = pallet.selected_bg, gui = '' },
    error_selected      = { guifg = pallet.error_fg,    guibg = pallet.selected_bg, gui = '' },
    info_selected       = { guifg = pallet.info_fg,     guibg = pallet.selected_bg, gui = '' },

    elem_selected       = { guifg = pallet.selected_fg, guibg = pallet.selected_bg },
    separator_selected  = { guifg = pallet.selected_bg, guibg = pallet.selected_bg },
    pick_selected       = { guifg = pallet.pick_fg,     guibg = pallet.selected_bg },
    separator           = { guifg = pallet.selected_bg, guibg = pallet.elem_bg },
    elem                = { guifg = pallet.elem_fg,     guibg = pallet.elem_bg },
    elem_inactive       = { guifg = pallet.elem_fg,     guibg = pallet.elem_bg },
    pick                = { guifg = pallet.pick_fg,     guibg = pallet.elem_bg },
    bar                 = { guifg = pallet.bar_fg,      guibg = pallet.bar_bg }
}


--  MAIN: Bufferline setup, using CoC diagnostics

return bufferline.setup {
    options = {
        diagnostics_indicator = function(count)
            return '['..count..']'
        end,

        offsets = {{
            filetype = 'NvimTree',
            text = 'File Explorer'
        }},

        max_prefix_length = 20, -- prefix used when a buffer is de-duplicated
        max_name_length = 20,
        indicator_icon = ' ',
        diagnostics = 'coc',

        show_buffer_close_icons = false,
        show_tab_indicators = true,
        show_buffer_icons = true,   -- disable filetype icons for buffers
        show_close_icon = false,

        -- can also be a table containing 2 custom separators
        -- [focused and unfocused]. eg: { '|', '|' }

        separator_style = { '', '' },
        always_show_bufferline = true,
        enforce_regular_tabs = true
    },


    --  SECTION: Colorshceme configuration

    highlights = {
        separator_selected      = colors.separator_selected,
        pick_selected           = colors.pick_selected,
        close_button_selected   = colors.elem_selected,
        duplicate_selected      = colors.elem_selected,
        modified_selected       = colors.elem_selected,
        buffer_selected         = colors.elem_selected,
        buffer_visible          = colors.elem_inactive,
        tab_selected            = colors.elem_selected,
        background              = colors.elem_inactive,
        separator_visible       = colors.separator,
        separator               = colors.separator,
        warning_visible         = colors.warning,
        warning                 = colors.warning,
        error                   = colors.error,
        pick                    = colors.pick,
        close_button_visible    = colors.elem,
        duplicate_visible       = colors.elem,
        modified_visible        = colors.elem,
        close_button            = colors.elem,
        duplicate               = colors.elem,
        modified                = colors.elem,
        tab                     = colors.elem,
        hint_visible            = colors.info,
        info_visible            = colors.info,
        hint                    = colors.info,
        info                    = colors.info,
        tab_close               = colors.bar,
        fill                    = colors.bar,

        warning_diagnostic_visible   = colors.warning,
        warning_diagnostic           = colors.warning,
        error_diagnostic             = colors.error,
        hint_diagnostic_visible      = colors.info,
        hint_diagnostic              = colors.info,
        info_diagnostic_visible      = colors.info,
        info_diagnostic              = colors.info,
        diagnostic_visible           = colors.info,
        diagnostic                   = colors.info
    }
}
