local status_ok, neotree = pcall(require, 'neo-tree')

if not status_ok then
    print '[ ERROR] Can\'t load nvim-neotree plugin'
    return
end


--  MAIN: NvimTree apparence settings

return neotree.setup {
    close_if_last_window = true,
    popup_border_style = 'rounded',
    enable_git_status = true,
    enable_diagnostics = false,
    sort_case_insensitive = false,
    sort_function = nil ,

    default_component_configs = {
        container = {
            enable_character_fade = false
        },

        indent = {
            indent_size = 2,
            padding = 1,
            with_markers = true, -- marker line config
            indent_marker = '│',
            last_indent_marker = '└',
            highlight = '',
            with_expanders = false, -- if nil and file nesting is enabled, will enable expanders
            expander_collapsed = '',
            expander_expanded = ' ',
            expander_highlight = 'NeoTreeExpander',
        },

        icon = {
            folder_closed = '',
            folder_open = '',
            folder_empty = 'ﰊ',
            default = '*', -- The next two settings are only a fallback, if you use nvim-web-devicons and configure default icons there then these will never be used.
            highlight = 'NeoTreeFileIcon'
        },

        modified = {
            symbol = '',
            highlight = 'NeoTreeModified',
        },

        name = {
            trailing_slash = false,
            use_git_status_colors = true,
            highlight = 'NeoTreeFileName',
        },

        git_status = {
            symbols = {
                added     = '✚', -- or "✚", but this is redundant info if you use git_status_colors on the name
                modified  = '', -- or "", but this is redundant info if you use git_status_colors on the name
                deleted   = '✖',-- this can only be used in the git_status source
                renamed   = '',-- this can only be used in the git_status source
                untracked = '', -- Status type
                ignored   = '',
                unstaged  = '',
                staged    = '',
                conflict  = '',
            }
        },
    },

    window = {
        position = 'left',
        width = 40,

        mapping_options = {
            noremap = true,
            nowait = true,
        },

        mappings = {
            ['<2-LeftMouse>'] = 'open',
            ['<cr>'] = 'open',
            ['o'] = 'open',
            ['s'] = 'open_split',
            ['i'] = 'open_vsplit',
            ['t'] = "open_tabnew",
            ['C'] = 'close_node',
            ['z'] = 'close_all_nodes',
            ['Z'] = 'expand_all_nodes',
            ['A'] = 'add_directory', -- also accepts the optional config.show_path option like "add".
            ['d'] = 'delete',
            ['r'] = 'rename',
            ['y'] = 'copy_to_clipboard',
            ['x'] = 'cut_to_clipboard',
            ['p'] = 'paste_from_clipboard',
            ['c'] = 'copy', -- takes text input for destination, also accepts the optional config.show_path option like 'add':
            ['m'] = 'move', -- takes text input for destination, also accepts the optional config.show_path option like 'add'.
            ['q'] = 'close_window',
            ['R'] = 'refresh',
            ['?'] = 'show_help',

            ['<space>'] = {
                'toggle_node',
                nowait = false, -- disable `nowait` if you have existing combos starting with this char that you want to use 
            },

            ['a'] = {
                'add', -- some commands may take optional config options, see `:h neo-tree-mappings` for details
                config = {
                    show_path = 'none' -- 'none', 'relative', 'absolute'
                }
            },
        }
    },

    filesystem = {
        follow_current_file = false, -- This will find and focus the file in the active buffer every
        group_empty_dirs = true, -- when true, empty folders will be grouped together
        hijack_netrw_behavior = 'open_default', -- netrw disabled, opening a directory opens neo-tree
        use_libuv_file_watcher = false, -- This will use the OS level file watchers to detect changes

        filtered_items = {
            visible = false, -- when true, they will just be displayed differently than normal items
            hide_dotfiles = false,
            hide_gitignored = false,

            hide_by_name = {
                'node_modules',
                '.git'
            },

            hide_by_pattern = {}, -- uses glob style patterns
        },

        window = {
            mappings = {
                ['<bs>'] = 'navigate_up',
                ['.'] = 'set_root',
                ['H'] = 'toggle_hidden',
                ['/'] = 'fuzzy_finder',
                ['D'] = 'fuzzy_finder_directory',
                ['f'] = 'filter_on_submit',
                ['<c-x>'] = 'clear_filter',
                ['[g'] = 'prev_git_modified',
                [']g'] = 'next_git_modified',
            }
        }
    },

    buffers = {
        follow_current_file = true, -- This will find and focus the file in the active buffer every
        group_empty_dirs = true, -- when true, empty folders will be grouped together
        show_unloaded = true,

        window = {
            mappings = {
                ['bd'] = 'buffer_delete',
                ['<bs>'] = 'navigate_up',
                ['.'] = 'set_root',
            }
        },
    },

    git_status = {
        window = {
            position = 'float',
            mappings = {
                ['A']  = 'git_add_all',
                ['gu'] = 'git_unstage_file',
                ['ga'] = 'git_add_file',
                ['gr'] = 'git_revert_file',
                ['gc'] = 'git_commit',
                ['gp'] = 'git_push',
                ['gg'] = 'git_commit_and_push',
            }
        }
    }
}
