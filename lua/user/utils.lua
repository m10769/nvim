local Utils = {}


--  DOC: Allow to make a mapping for multiple modes at one time
--  NOTE: local vim_default_modes = { 'n', 'v', 's', 'x', 'o', '!', 'i', 'l', 'c', 't' }

Utils.map = function(mode, keys, command, opts)
    if not opts then --  DESC: Setup the default value if opts argument isn't defined
        opts = {
            noremap = true, silent = true
        }
    end

    if type(mode) == 'string' then
        vim.api.nvim_set_keymap(mode, keys, command, opts)

    elseif type(mode) == 'table' then
        for _, element in ipairs(mode) do
            vim.api.nvim_set_keymap(element, keys, command, opts)
        end
    end
end


--  DOC: Setup the vim options based on a table

Utils.options = function(table)
    for key, value in pairs(table) do
        vim.opt[key] = value
    end
end


return Utils
