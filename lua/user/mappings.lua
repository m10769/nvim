local status_ok, Utils = pcall(require, 'user.utils')

if not status_ok then
    print '[ ERROR] Can\'t find utils module'
    return --  DESC: terminate this process
end

local map = Utils.map


--  HACK: Just to run map() in a massive table with all the normal mappings

local function normal_map(table)
    for _, element in pairs(table) do
        map('n', element[1], element[2])
    end
end


--  MAIN: Vim mappings created by Utils.map() function

vim.g.mapleader = ' '

map({ 'n', 'i', 'v' }, '<c-s>', '<esc>:w!<cr>') -- Save the file
map({ 'v', 'i', 's' }, '<c-l>', '<esc>') -- Better <esc>

map('i', '<c-k>', '<cr><esc>O') -- Tripple return
map('i', '<c-p>', '<C-w>') -- Delete an word
map('t', ',,', '<cr>exit<cr>') -- Exit from ToggleTerm terminal

map({ 'n', 'v' }, '<cr>', ':Commentary<cr>') -- Vim commentary

map('n', '<leader>MM', ':mksession! ~/.local/vim_sessions/', {
    noremap = true, silent = false
}) -- Better way to save a vim session

map('n', '<leader>S', ':%s/', {
    noremap = true, silent = false
}) -- Better subistitute keybind


--  INFO: Normal mode and plugin keybindings

normal_map {
    { '<leader>1', ':setl expandtab tabstop=2 shiftwidth=2 softtabstop=2<cr>' }, -- Change indent spaces width
    { '<leader>2', ':setl expandtab tabstop=4 shiftwidth=4 softtabstop=4<cr>' },
    { '<leader>3', ':setl expandtab tabstop=8 shiftwidth=8 softtabstop=8<cr>' },

    { '<leader><c-w>',   ':setl wrap!<cr>'  }, -- Toggle word wrap
    { '<leader>:', ':set number!<cr>' }, -- Toggle line numbers

    { '<leader><s-tab>', ':tabprevious<cr>' }, -- Vim tab navigation
    { '<leader><tab>',   ':tabnext<cr>' },

    { '<leader>t', ':tabnew<cr>' }, -- Create a new tab

    { '<leader><down>',  ':bdelete<cr>' }, -- Buffer management and navigation
    { '<leader><up>',    ':ls<cr>' },
    { '<s-tab>',         ':bp<cr>' },
    { '<tab>',           ':bn<cr>' },

    { '<leader>h', '<c-w>h' }, -- Window splits navigation
    { '<leader>j', '<c-w>j' },
    { '<leader>k', '<c-w>k' },
    { '<leader>l', '<c-w>l' },

    { '<right>', ':vertical resize -1<cr>' }, -- Window resize
    { '<left>',  ':vertical resize +1<cr>' },
    { '<down>',  ':resize -1<cr>' },
    { '<up>',    ':resize +1<cr>' },

    { '<leader>s', ':vsplit<cr>' }, -- Window splits
    { '<leader>i', ':split<cr>' },

    { '<leader>Q', ':qa!<cr>' }, -- Quit editor
    { '<leader>q', ':q<cr>' },

    { '<leader>;', 'A;<esc>' }, -- Append a ; at the end of the line
    { '<leader>,', 'A,<esc>' }, -- Append a , at the end of the line
    { '<leader>.', 'A.<esc>' }, -- Append a . at the end of the line

    { '<leader>gg', 'mZgg=G`Z' }, -- Auto indent all lines

    { 'Y', 'yg$' }, -- Use Y to copy all the next characters after cursor position

    { 'j', 'gj' }, -- Navigate using visual lines
    { 'k', 'gk' },

    { '<leader><leader>', '@' }, -- Better @ to run a macro
    { '<c-m>', '"' }, -- Better " to go to a mark

    { '<leader>D', ':read !date +"\\%d/\\%m/\\%Y"<cr>kJ' }, -- Write date

    --  SECTION: Plugins mappings. Telescope, ToggleTerm and Coc Actions

    { '<leader>FGC', ':lua require "telescope.builtin".git_commits()<cr>' },
    { '<leader>FGS', ':lua require "telescope.builtin".git_status()<cr>' },
    { '<leader>FS', ':lua require "telescope.builtin".grep_string()<cr>' },
    { '<leader>FF', ':lua require "telescope.builtin".find_files()<cr>' },
    { '<leader>FB', ':lua require "telescope.builtin".buffers()<cr>' },
    { '<leader>FM', ':lua require "telescope.builtin".marks()<cr>' },

    { '<leader>T', ':ToggleTerm<cr>' },

    { '<leader>n', ':NeoTreeFloatToggle<cr>' },
    { '<leader>N', ':NeoTreeShowToggle<cr>' },

    { '<leader><c-k>', ':call CocActionAsync("jumpDefinition")<cr>' },
    { '<leader>cc',    ':call CocActionAsync("pickColor")<cr>' },
    { '<leader>R',     ':call CocActionAsync("rename")<cr>' }
}
