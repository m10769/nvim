local status_ok, Utils = pcall(require, 'user.utils')

if not status_ok then
    print '[ ERROR] Can\'t find utils module'
    return
end

local options = Utils.options


--  HACK: Add a custom ident when the buffer with a especifc file type loads

local function custom_indent(width, files)
    local command_template = 'au BufWinEnter %s setl shiftwidth=%i tabstop=%i expandtab'

    for _, element in ipairs(files) do
        local command_str = string.format(command_template, element, width, width)
        vim.cmd(command_str)
    end
end


--  MAIN: Main vim option config (set command in vimscript)

options {
    backup = false,
    clipboard = 'unnamedplus',
    ignorecase = true,

    encoding = 'UTF-8',
    termguicolors = true,
    timeoutlen = 500,

    hidden = true,
    confirm = true,
    cursorline = true,
    inccommand = 'split',
    mouse = 'a',
    showmode = false,

    number = true,
    numberwidth = 6,
    splitbelow = true,
    splitright = true,

    expandtab = true,
    smartindent = false,
    shiftwidth = 4,
    tabstop = 4,
    breakindent = true,
    formatoptions = '1',
    linebreak = true,
    wrap = false
}


--  INFO: Use custom indent sizes to some file types

custom_indent(2, { '*.html', '*.json', '*.jsx', '*.tsx' })
custom_indent(8, { '*.cc', '*.java' })


--  WARN: Vimscript

vim.cmd [[ au BufWinEnter *.txt set ft=help ]] --  DESC: Use vim wiki syntaxe for plain text files
vim.cmd [[ set nocompatible path+=** ]]        --  DESC: Some options that dont work on lua...
vim.cmd [[ set fcs=eob:\ ]]                    --  DESC: Remove the ~'s at the end of the buffers
