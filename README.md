# My custom Neovim config

# What I'm using right now?

+ **Line**: 📦 [`lualine`](https://github.com/nvim-lualine/lualine.nvim)
+ **Buffer line**: 📦 [`bufferline`](https://github.com/akinsho/bufferline.nvim)
+ **File explore**: 📦 [`NvimTree`](https://github.com/kyazdani42/nvim-tree.lua)
+ **Color scheme**: 📦 [`onenord`](https://github.com/rmehri01/onenord.nvim)
+ **Fuzzy finder**: 📦 [`telescope`](https://github.com/nvim-telescope/telescope.nvim)
+ Check out my ⌨️ [mappings](https://gitlab.com/m10769/nvim/-/blob/main/lua/user/mappings.lua)! I'm really pround of them _UwU_


### Awesome plugins that I found lately

+ 📦 [`todo-comments`](https://github.com/folke/todo-comments.nvim)
+ 📦 [`gitsigns`](https://github.com/lewis6991/gitsigns.nvim)


## More info

That's my personal (may not work in your machine) Neovim config. I created this repo to learn new programming languages and work with front-end stuff – Javascript, Typescript, Scss/Sass, React, etc. The language servers was setted up with [**CoC.nvim**](https://github.com/neoclide/coc.nvim), fell free to borrow some of my code. _;3_

Also, I learned alot making this config, and thats the most important thing to me, hope you enjoy. :sparkles:


![Workflow example](docs/neovim_demo.jpg)


**Useful configs that helped me alot**

- [Reddit post](https://www.reddit.com/r/neovim/comments/se377t/telescopenvim_looks_neat/) that helped me to theme the Telescope
- [NvChad](https://github.com/NvChad/NvChad), an awesome Neovim configuration
- [Neovim from Scratch](https://youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ)


### Instalation

Install [**Packer.nvim**](https://github.com/wbthomason/packer.nvim):

```shell
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
    ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

[**CoC.nvim**](https://github.com/neoclide/coc.nvim) dependencies:

```shell
sudo pacman -Syyu nodejs npm yarn # I use Arch, BTW...
```



<details>
<summary><h3>CoC extensions 📦</h3></summary>
<ul>
<li><code>coc-snippets</code></li>
<li><code>coc-html-css-support</code></li>
<li><code>coc-html</code></li>
<li><code>coc-highlight</code></li>
<li><code>coc-discord</code></li>
<li><code>coc-cssmodules</code></li>
<li><code>coc-tsserver</code></li>
<li><code>coc-sumnekolua</code></li>
<li><code>coc-sh</code></li>
<li><code>coc-rls</code></li>
<li><code>coc-python</code></li>
<li><code>coc-json</code></li>
<li><code>coc-jedi</code></li>
<li><code>coc-htmlhint</code></li>
<li><code>coc-go</code></li>
<li><code>coc-discordrpc</code></li>
<li><code>coc-css</code></li>
<li><code>coc-clangd</code></li>
</ul>
</details>
